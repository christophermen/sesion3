package com.aeditip.introspringbcd.wc2018.entidades.partido;

/**
 * Clase embebida en la entidad Partido.
 * Historial de versiones
 *   1.0 (27/03/2021): Creación de la clase.
 *   1.1 (28/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.1
 */

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

import com.aeditip.introspringbcd.wc2018.entidades.Pais;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// Anotaciones de la librería Lombok para la automatización de constructores y métodos iniciales.
@Data
@NoArgsConstructor
@AllArgsConstructor
// Anotación que indica que esta clase es embebida.
@Embeddable
public class ResultadoPais {
	@ManyToOne
	Pais pais;
	int goles = 0;
	int golesPenal = 0;
	int amarillas = 0;
	int segundas = 0;
	int rojas = 0;
	
	/* Constructor de la clase, usando como parámetro un objeto de la entidad País.
	 * @param	pais	Objeto de entidad País para enmascarar.
	 */
	public ResultadoPais(Pais pais) {
		this.pais = pais;
	}
}
