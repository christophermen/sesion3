package com.aeditip.introspringbcd.wc2018.entidades;

/**
 * Clase que define la entidad para la tabla Partido.
 * Historial de versiones
 *   1.0 (20/03/2021): Creación de la clase.
 *   1.1 (21/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 *   1.2 (27/03/2021): Agregación de clases embebidas y modificación de métodos para el soporte
 *   					de los cambios.
 *   1.3 (28/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.3
 */

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import com.aeditip.introspringbcd.wc2018.entidades.partido.Organizacion;
import com.aeditip.introspringbcd.wc2018.entidades.partido.ResultadoPais;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// Anotaciones de la librería Lombok para la automatización de constructores y métodos iniciales.
@Data
@NoArgsConstructor
@AllArgsConstructor
// Especificación de entidad.
@Entity
// Relación con la Tabla "partido" en la base de datos.
@Table(name = "partido")
public class Partido {
	@Id
	// Anotación para definir la estrategia para la modificación del campo.
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", columnDefinition = "INT(8)")
	private int id;
	
	// Introducción a las clases embebidas
	// Esta anotación indica que el atributo es embebido
	@Embedded
	// La siguiente anotación lista los atributos embebidos relacionados con llaves foráneas
    @AssociationOverrides(value = {
        @AssociationOverride( name = "edicion", joinColumns = @JoinColumn(name = "id_edicion")),
        @AssociationOverride( name = "fase", joinColumns = @JoinColumn(name = "id_fase"))
    })
	private Organizacion organizacion;
	
	// Clase embebida para los datos del país local
	@Embedded
    @AssociationOverrides(value = {
        @AssociationOverride( name = "pais", joinColumns = @JoinColumn(name = "id_local"))
    })
	// La siguiente anotación lista los atributos embebidos relacionados con atributos normales.
	@AttributeOverrides(value = {
	    @AttributeOverride( name = "goles", column = @Column(name = "marcador_local")),
	    @AttributeOverride( name = "golesPenal", column = @Column(name = "penales_local")),
	    @AttributeOverride( name = "amarillas", column = @Column(name = "amarilla_local")),
	    @AttributeOverride( name = "segundas", column = @Column(name = "segunda_local")),
	    @AttributeOverride( name = "rojas", column = @Column(name = "roja_local"))
	})
	private ResultadoPais local;
	
	// Clase embebida para los datos del país visitante
	@Embedded
    @AssociationOverrides(value = {
        @AssociationOverride( name = "pais", joinColumns = @JoinColumn(name = "id_visitante"))
    })
	@AttributeOverrides(value = {
	    @AttributeOverride( name = "goles", column = @Column(name = "marcador_visitante")),
	    @AttributeOverride( name = "golesPenal", column = @Column(name = "penales_visitante")),
	    @AttributeOverride( name = "amarillas", column = @Column(name = "amarilla_visitante")),
	    @AttributeOverride( name = "segundas", column = @Column(name = "segunda_visitante")),
	    @AttributeOverride( name = "rojas", column = @Column(name = "roja_visitante"))
	})
	private ResultadoPais visitante;
	
	@Column(name = "jugado", columnDefinition = "INT(1)")
	private int disputado;
	
	/* Constructor personalizado de la clase, en el cual sólo se involucran cuatro (04) atributos
	 * 	de los (07) presentes.
	 */
	public Partido(Organizacion organizacion, Pais local, Pais visitante) {
		this.organizacion = organizacion;
		this.local = new ResultadoPais(local);
		this.visitante = new ResultadoPais(visitante);
		this.disputado = 0;
	}
	
	/* Calcula la diferencia de goles del país local. Para calcular la diferencia del
	 * 	país visitante deberá negativizar este valor.
	 * @return		Diferencia de goles de un partido.
	 * Tabla de valores:
	 * - Negativo	: si ganó el visitante.
	 * - Neutral (0): si hubo empate.
	 * - Positivo	: si ganó el local.
	 */
	public int determinarDiferenciaGoles() {
		return local.getGoles()-visitante.getGoles();
	}
	
	/* Devuelve el país que ganó el partido.
	 * @return		País ganador del partido. Si ocurrió un empate, devolverá null.
	 */
	public Pais obtenerGanador() {
		if(determinarDiferenciaGoles() > 0)
			return this.local.getPais();
		else if(determinarDiferenciaGoles() < 0)
			return this.visitante.getPais();
		else
			return null;
	}
}
