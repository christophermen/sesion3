package com.aeditip.introspringbcd.wc2018.entidades;

/**
 * Clase que define la entidad para la tabla Participante.
 * Historial de versiones
 *   1.0 (27/03/2021): Creación de la clase.
 *   1.1 (28/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.1
 */

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

// Anotaciones de la librería Lombok para la automatización de constructores y métodos iniciales.
@Data
@NoArgsConstructor
// Especificación de entidad.
@Entity
// Relación con la Tabla "participacion" en la base de datos.
@Table(name = "participacion")
public class Participacion {
	// Anotación para llaves primarias compuestas, en lugar de @Id.
	@EmbeddedId
	ClaveParticipacion id;

	@ManyToOne
	// Anotación para mapear la llave principal de la segunda tabla.
    @MapsId("id_pais")
	@JoinColumn(name = "id_pais")
    Pais pais;
	
	@ManyToOne
    @MapsId("id_edicion")
	@JoinColumn(name = "id_edicion")
    Edicion edicion;
	
	@Column(name = "grupo")
	String grupo;
	
	public Participacion(Pais pais, Edicion edicion, String grupo) {
		// Establecimiento de la llave primaria de esta tabla.
		this.id = new ClaveParticipacion(pais.getCodigo(), edicion.getAnho());
		this.pais = pais;
		this.edicion = edicion;
		this.grupo = grupo;
	}
}
