package com.aeditip.introspringbcd.wc2018.entidades;

/**
 * Clase que define la entidad para la tabla Pais.
 * Historial de versiones
 *   1.0 (20/03/2021): Creación de la clase.
 *   1.1 (21/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 *   1.2 (27/03/2021): Agregación del atributo "participaciones".
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.2
 */

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// Anotaciones de la librería Lombok para la automatización de constructores y métodos iniciales.
@Data
@NoArgsConstructor
@AllArgsConstructor
// Especificación de entidad.
@Entity
// Relación con la Tabla "pais" en la base de datos.
@Table(name = "pais")
public class Pais {
	// Anotación que indica que este atributo es el identificador principal (en caso de una base
	//	de datos relacional, la llave primaria). Para llaves primarias compuestas, revise la docu-
	// 	mentación de Spring.
	@Id
	// Anotación que indica que este atributo corresponde al nombre que indique en la opción "name"
	// Si su proyecto tiene la configuración "spring.jpa.hibernate.ddl-auto" (en el archivo
	//	src/main/resources/application.properties) en none, la opción "columnDefinition" no es
	//	necesaria
	@Column(name = "codigo", columnDefinition = "CHAR(3)")
	// Procure que el tipo de dato en este proyecto esté relacionado al tipo de dato declarado en
	//	la estructura de su base de datos.
	private String codigo;
	
	@Column(name = "nombre", columnDefinition = "VARCHAR(100)")
	private String nombre;
	
	// La anotación @OneToMany es la contraparte de @ManyToOne. Obtendrá todos los objetos que se
	//	mapea el atributo descrito en el parámetro "mappedBy" de la anotación.
	// Los elementos listados en este componente List corresponden a la consulta de base de datos
	//	SELECT * FROM otra_tabla WHERE fk_otra_tabla_en_esta_tabla = pk_esta_tabla
	// Por ejemplo, para esta tabla: SELECT * FROM participacion WHERE id_pais = <this.codigo>
	@OneToMany(mappedBy = "pais", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Participacion> participaciones = new ArrayList<Participacion>();
}
