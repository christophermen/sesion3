package com.aeditip.introspringbcd.wc2018.entidades;

/**
 * Clase que define la entidad para la tabla Fase de Partido.
 * Historial de versiones
 *   1.0 (20/03/2021): Creación de la clase.
 *   1.1 (21/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.1
 */

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// Anotaciones de la librería Lombok para la automatización de constructores y métodos iniciales.
@Data
@NoArgsConstructor
@AllArgsConstructor
// Especificación de entidad
@Entity
// Relación con la Tabla "fase_partido" en la base de datos.
@Table(name = "fase_partido")
public class FasePartido {
	@Id
	@Column(name = "id", columnDefinition = "INT(1)")
	private int id;
	
	@Column(name = "nombre", columnDefinition = "VARCHAR(100)")
	private String nombre;
}
