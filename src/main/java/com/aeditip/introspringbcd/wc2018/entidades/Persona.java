package com.aeditip.introspringbcd.wc2018.entidades;

/**
 * Clase que define la entidad para la tabla Persona.
 * Historial de versiones
 *   1.0 (27/03/2021): Creación de la clase.
 *   1.1 (28/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.1
 */

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// Anotaciones de la librería Lombok para la automatización de constructores y métodos iniciales.
@Data
@NoArgsConstructor
@AllArgsConstructor
// Especificación de entidad.
@Entity
// Relación con la Tabla "persona" en la base de datos.
@Table(name = "persona")
// Esta clase es la superclase de la clase Jugador, por lo que usamos la anotación @Inheritance.
// Esta anotación define tres estrategias para el manejo de herencia. En este caso, usaremos la
//	estrategia JOINED.
@Inheritance(strategy = InheritanceType.JOINED)
public class Persona {
	@Id
	@Column(name = "id")
	private int id;
	
	@Column(name = "nombre")
	private String nombre;
	
	// Anotación que se usa campos involucrados en llaves foráneas con una
	//	relación de 1xM.
	@ManyToOne
	// Anotación para indicar que este atributo corresponde a una columna relacionada
	//	a una llave foránea.
	@JoinColumn(name = "id_pais")
	private Pais pais;
}
