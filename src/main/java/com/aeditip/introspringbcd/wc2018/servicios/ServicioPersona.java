package com.aeditip.introspringbcd.wc2018.servicios;

/**
 * Clase que define una estereotipo de servicio estándar de Spring para la clase Pais.
 * Historial de versiones
 *   1.0 (20/03/2021): Creación de la clase.
 *   1.1 (21/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.1
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.aeditip.introspringbcd.wc2018.entidades.Jugador;
import com.aeditip.introspringbcd.wc2018.entidades.Persona;
import com.aeditip.introspringbcd.wc2018.repositorios.RepositorioJugador;
import com.aeditip.introspringbcd.wc2018.repositorios.RepositorioPersona;

@Service
public class ServicioPersona {
	// Declaración de repositorios.
	@Autowired
	private RepositorioPersona repoPersona;
	@Autowired
	private RepositorioJugador repoJugador;
	
	/* Obtiene una persona de la base de datos, dado un identificador determinado.
	 * @param	id	Identificador de la persona registrado en la base de datos. 
	 * @return		Un objeto de la clase Persona con los datos registrados en la base de datos.
	 */
	public Persona obtenerPersonaEspecifica(@PathVariable int id){
		return repoPersona.findById(id).get();
	}
	
	/* Obtiene un jugador de la base de datos, dado un identificador determinado.
	 * @param	id	Identificador del jugador registrado en la base de datos. 
	 * @return		Un objeto de la clase Jugador con los datos registrados en la base de datos.
	 */
	public Jugador obtenerJugadorEspecifica(@PathVariable int id){
		return repoJugador.findById(id).get();
	}
}
