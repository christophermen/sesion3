package com.aeditip.introspringbcd.wc2018.servicios;

/**
 * Clase que define una estereotipo de servicio estándar de Spring para la clase Partido.
 * Historial de versiones
 *   1.0 (27/03/2021): Creación de la clase.
 *   1.1 (28/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.1
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.aeditip.introspringbcd.wc2018.dto.DTOPartido;
import com.aeditip.introspringbcd.wc2018.entidades.FasePartido;
import com.aeditip.introspringbcd.wc2018.entidades.Pais;
import com.aeditip.introspringbcd.wc2018.entidades.Partido;
import com.aeditip.introspringbcd.wc2018.entidades.partido.Organizacion;
import com.aeditip.introspringbcd.wc2018.entidades.partido.ResultadoPais;
import com.aeditip.introspringbcd.wc2018.repositorios.RepositorioEdicion;
import com.aeditip.introspringbcd.wc2018.repositorios.RepositorioFasePartido;
import com.aeditip.introspringbcd.wc2018.repositorios.RepositorioPais;
import com.aeditip.introspringbcd.wc2018.repositorios.RepositorioPartido;

@Service
public class ServicioPartido {
	// Declaración de repositorios.
	@Autowired
	private RepositorioEdicion repoEdicion;
	@Autowired
	private RepositorioPartido repoPartido;
	@Autowired
	private RepositorioFasePartido repoFase;
	@Autowired
	private RepositorioPais repoPais;
	
	/* Obtiene un partido de la base de datos, dado un identificador determinado.
	 * @param	id		Identificador del país 
	 * @return			Un objeto Partido con los datos registrados en la base de datos.
	 */
	public DTOPartido obtenerPartidoEspecifico(@PathVariable int id){
		return new DTOPartido(repoPartido.findById(id).get());
	}
	
	/* Agrega un partido a la base de datos.
	 * @param	partido	Partido a agregar.
	 * @return			Un objeto Partido con los datos registrados en la base de datos.
	 */
	public Partido agregarPartido(@RequestParam int idFase, @RequestParam String idLocal, @RequestParam String idVisitante) {
		// Crear el objeto a devolver
		Partido auxPartido = new Partido();		
		// Obtener los objetos de BD correspodientes a las llaves foráneas.
		FasePartido fase = repoFase.findById(idFase).get();
		Pais local = repoPais.findById(idLocal).get();
		Pais visitante = repoPais.findById(idVisitante).get();
		// "Armar" el objeto
		auxPartido.setOrganizacion(new Organizacion(repoEdicion.findById(2018).get(), fase));
		auxPartido.setLocal(new ResultadoPais(local));
		auxPartido.setVisitante(new ResultadoPais(visitante));
		// Devolver el objeto de partido
		return repoPartido.save(auxPartido);
	}
	
	/* Edita un partido para registrar los resultados a la base de datos, dados los datos necesarios para editar.
	 * @param	id			Identificador de partido.
	 * @param	golLocal	Número de goles anotados por el equipo local.
	 * @param	golVisi		Número de goles anotados por el equipo visitante.
	 * @return				Un objeto Partido con los datos modificados en la base de datos.
	 */
	public Partido registrarResultado(@PathVariable int id, @RequestParam int golLocal, @RequestParam int golVisi) {
		if(repoPartido.findById(id).isPresent()) {
			Partido auxPartido = repoPartido.findById(id).get();
			ResultadoPais local = auxPartido.getLocal(), visitante = auxPartido.getVisitante();
			local.setGoles(golLocal);
			visitante.setGoles(golVisi);
			auxPartido.setLocal(local);
			auxPartido.setVisitante(visitante);
			auxPartido.setDisputado(1);
			return repoPartido.save(auxPartido);
		}
		return null;
	}
	
	/* Elimina de manera permanente un partido a la base de datos, dado el identificador del partido a eliminar.
	 * @param	id	Identificador del país.
	 */
	public void eliminarPartido(@PathVariable int id) {
		// Si encuentra el partido, podrá eliminarlo de la base de datos.
		if(repoPartido.findById(id).isPresent())
			repoPartido.deleteById(id);
	}
}
