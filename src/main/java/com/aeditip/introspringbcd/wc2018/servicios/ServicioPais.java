package com.aeditip.introspringbcd.wc2018.servicios;

/**
 * Clase que define una estereotipo de servicio estándar de Spring para la clase Pais.
 * Historial de versiones
 *   1.0 (20/03/2021): Creación de la clase.
 *   1.1 (21/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 *   1.2 (27/03/2021): Modificación según el modelo de base de datos.
 *   1.3 (28/03/2021): Agregación de comentarios para explicar los procedimientos utilizados. 
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.3
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.aeditip.introspringbcd.wc2018.dto.DTOPais;
import com.aeditip.introspringbcd.wc2018.entidades.Pais;
import com.aeditip.introspringbcd.wc2018.errores.nfex.PaisNFEx;
import com.aeditip.introspringbcd.wc2018.repositorios.RepositorioPais;

// Anotación para anotar esta clase como un servicio estereotipado de Spring para la entidad País.
@Service
public class ServicioPais {
	// Declaración de repositorios a utilizar.
	@Autowired
	private RepositorioPais repoPais;
	
	/* "Traduce" un objeto DTO de País en un objeto de entidad de País.
	 * @param	codigo	Codigo del país 
	 * @return			Un objeto Pais con los datos registrados en la base de datos.
	 */
	private Pais traductor(DTOPais oPais) {
		// Crear una objeto de clase País.
		Pais auxPais = new Pais();
		auxPais.setCodigo(oPais.getCodigo());
		auxPais.setNombre(oPais.getDenominacion());
		return auxPais;
	}
	
	/* Obtiene un país de la base de datos, dado un código determinado.
	 * @param	codigo	Código del país 
	 * @return			Un objeto Pais con los datos registrados en la base de datos.
	 */
	public DTOPais obtenerPaisEspecifico(@PathVariable String codigo){
		// Usando .isPresent()
		if(repoPais.findById(codigo).isPresent())
			return new DTOPais(repoPais.findById(codigo).get());
		throw new PaisNFEx(codigo);
		// Usando .isEmpty()
		/*if(repoPais.findById(codigo).isEmpty())
			throw new PaisNFEx(codigo);
		else
			return new DTOPais(repoPais.findById(codigo).get());*/
	}
	
	/* Agrega un país a la base de datos.
	 * @param	pais	Objeto DTO del país a agregar.
	 * @return			Un objeto DTO de País con los datos registrados en la base de datos.
	 */
	public DTOPais agregarPais(@RequestBody DTOPais pais) {
		Pais auxPais = traductor(pais);
		return new DTOPais(repoPais.save(auxPais));
	}
	
	/* Edita un país a la base de datos, dados los datos modificados y el código del país
	 * 	a editar.
	 * @param	pais	Objeto DTO del país a editar.
	 * @param	codigo	Codigo del país.
	 * @return			Un objeto Pais con los datos modificados en la BD.
	 */
	public DTOPais editarPais(@RequestBody DTOPais pais, @PathVariable String codigo) {
		// Usando .isPresent()
		if(repoPais.findById(codigo).isPresent())
			return new DTOPais(repoPais.save(traductor(pais)));
		throw null;
		// Usando .isEmpty()
		/*if(repoPais.findById(codigo).isEmpty())
			return null;
		return new DTOPais(repoPais.save(traductor(pais)));*/
	}
	
	/* Elimina de manera permanente un país a la base de datos, dado el código del país
	 * 	a eliminar.
	 * @param	codigo	Codigo del país.
	 */
	public void eliminarPais(@PathVariable String codigo) {
		if(repoPais.findById(codigo).isPresent())
			repoPais.deleteById(codigo);
	}
}
