package com.aeditip.introspringbcd.wc2018.errores.nfex;

/**
 * Clase que define una excepción personalizada en el caso de no encontrar un país en la base de datos.
 * Historial de versiones
 *   1.0 (20/03/2021): Creación de la clase.
 *   1.1 (21/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.1
 */

// La siguiente línea permite suprimir la advertencia de declarar un número serial a una clase serializable.
@SuppressWarnings("serial")
public class PaisNFEx extends RuntimeException {
	public PaisNFEx(String codigo) {
		super("No se ha podido encontrar el país con código " + codigo);
	}
}
