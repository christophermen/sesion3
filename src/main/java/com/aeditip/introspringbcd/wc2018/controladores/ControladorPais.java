package com.aeditip.introspringbcd.wc2018.controladores;

/**
 * Clase que define una estereotipo de controlador estándar de Spring para la clase Pais.
 * Historial de versiones
 *   1.0 (20/03/2021): Creación de la clase.
 *   1.1 (21/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 *   1.2 (27/03/2021): Modificación según el modelo de base de datos.
 *   1.3 (28/03/2021): Agregación de comentarios para explicar los procedimientos utilizados. 
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.3
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aeditip.introspringbcd.wc2018.dto.DTOPais;
import com.aeditip.introspringbcd.wc2018.servicios.ServicioPais;

// Anotación para anotar esta clase como un controlador estereotipado de Spring para la entidad Pais.
@RestController
// Ruta general de la ruta de los métodos que controla este objeto.
@RequestMapping("/api/paises")
//Solución de mecanismo CORS.
@CrossOrigin(origins="*", methods = {RequestMethod.GET, RequestMethod.POST,
		RequestMethod.PUT, RequestMethod.DELETE})
public class ControladorPais {
	// Declaración de servicio a utilizar.
	@Autowired
	private ServicioPais servicio;
	
	/* Solicita al servicio relacionado un país de la base de datos, dado un código determinado.
	 * Para acceder al servicio, deberá intentar acceder a la ruta:
	 *  Por ejemplo, si quiere consultar el país Alemania (GER), con el puerto configurado en 8081.
	 * 	<enlace del servidor donde está ejecutándose este proyecto>:8081/api/paises/GER (GET)
	 * @param	codigo	Codigo del país.
	 * @return			Un objeto DTO de País con los datos registrados en la base de datos.
	 */
	@RequestMapping(value = "{codigo}", method = RequestMethod.GET)
	public DTOPais obtenerPaisEspecifico(@PathVariable String codigo){
		return servicio.obtenerPaisEspecifico(codigo);
	}
	
	/* Solicita al servicio relacionado agregar un país a la base de datos.
	 * @param	pais	Objeto DTO del país a agregar.
	 * @return			Un objeto DTO de País con los datos registrados en la base de datos.
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public DTOPais agregarPais(@RequestBody DTOPais pais){
		return servicio.agregarPais(pais);
	}
	
	/* Solicita al servicio relacionado editar un país a la base de datos, dados los datos modificados y
	 * 	el código del país a editar.
	 * @param	pais	Objeto DTO del país a agregar.
	 * @param	codigo	Codigo del país.
	 * @return			Un objeto Pais con los datos modificados en la base de datos.
	 */
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public DTOPais editarPais(@RequestBody DTOPais pais, @PathVariable String codigo){
		return servicio.editarPais(pais, codigo);
	}
	
	/* Solicita al servicio relacionado eliminar de manera permanente un país a la base de datos, dado
	 * 	el código del país a eliminar.
	 * @param	codigo	Codigo del país.
	 */
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public void eliminarPais(@PathVariable String codigo){
		servicio.eliminarPais(codigo);
	}
}
