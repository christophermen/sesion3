package com.aeditip.introspringbcd.wc2018.controladores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aeditip.introspringbcd.wc2018.servicios.ServicioGeneral;

// Anotación para anotar esta clase como un controlador estereotipado de Spring para la entidad Riesgo.
@RestController
// Ruta general de la ruta de los métodos que controla este objeto.
@RequestMapping("/api/general")
public class ControladorGeneral {
	@Autowired
	private ServicioGeneral servicio;
	
	/* Solicita al servicio relacionado imprimir un mensaje de bienvenida.
	 */
	@RequestMapping(value = "hola", method = RequestMethod.GET)
	public String bienvenida() {
		return servicio.bienvenida();
	}
	
	/* Solicita al servicio relacionado generar los partidos de Octavos de Final de la Copa.
	 * @param	edicion		Año de la edición de la Copa Mundial.
	 */
	@RequestMapping(value = "fin-fase-grupos", method = RequestMethod.GET)
	public void finalizarFaseGrupos(@RequestParam int edicion){
		servicio.finalizarFaseGrupos(edicion);
	}
	
	/* Solicita al servicio relacionado generar los partidos de Cuartos de Final de la Copa.
	 * @param	edicion		Año de la edición de la Copa Mundial.
	 */
	@RequestMapping(value = "fin-octavos", method = RequestMethod.GET)
	public void finalizarOctavosdeFinal(@RequestParam int edicion){
		servicio.finalizarOctavosdeFinal(edicion);
	}
}
