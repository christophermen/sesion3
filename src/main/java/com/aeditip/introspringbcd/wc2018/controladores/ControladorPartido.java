package com.aeditip.introspringbcd.wc2018.controladores;

/**
 * Clase que define una estereotipo de controlador estándar de Spring para la clase Partido.
 * Historial de versiones
 *   1.0 (20/03/2021): Creación de la clase.
 *   1.1 (21/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.1
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aeditip.introspringbcd.wc2018.dto.DTOPartido;
import com.aeditip.introspringbcd.wc2018.entidades.Partido;
import com.aeditip.introspringbcd.wc2018.servicios.ServicioPartido;

// Anotación para anotar esta clase como un controlador estereotipado de Spring para la entidad Partido.
@RestController
// Ruta general de la ruta de los métodos que controla este objeto.
@RequestMapping("/api/partidos")
// Solución de mecanismo CORS.
@CrossOrigin(origins="*", methods = {RequestMethod.GET, RequestMethod.POST,
		RequestMethod.PUT, RequestMethod.DELETE})
public class ControladorPartido {
	// Declaración de servicio a utilizar.
	@Autowired
	private ServicioPartido servicio;
	
	/* Solicita al servicio relacionado un país de la base de datos, dado un identificador determinado.
	 * @param	id	Identificador del país.
	 * @return		Un objeto Pais con los datos registrados en la base de datos.
	 */
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public DTOPartido obtenerPartidoEspecifico(@PathVariable int id){
		return servicio.obtenerPartidoEspecifico(id);
	}
	
	/* Solicita al servicio agregar un partido a la base de datos.
	 * @param	idFase			Identificador de la fase del partido.
	 * @param	idLocal			Código del país local.
	 * @param	idVisitante		Código del país visitante.	
	 * @return					Un objeto Partido con los datos registrados en la base de datos.
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public Partido agregarPartido(@RequestParam int idFase, @RequestParam String idLocal, @RequestParam String idVisitante){
		return servicio.agregarPartido(idFase, idLocal, idVisitante);
	}
	
	/* Solicita al servicio relacionado editar un partido para registrar los resultados a la base de datos, dados los datos
	 * 	necesarios para editar.
	 * @param	id			Identificador de partido.
	 * @param	golLocal	Número de goles anotados por el equipo local.
	 * @param	golVisi		Número de goles anotados por el equipo visitante.
	 * @return				Un objeto Partido con los datos modificados en la base de datos.
	 */
	@RequestMapping(value = "{id}/resultado", method = RequestMethod.PUT)
	public Partido registrarResultado(@PathVariable int id, @RequestParam int golLocal, @RequestParam int golVisi){
		return servicio.registrarResultado(id, golLocal, golVisi);
	}
	
	/* Solicita al servicio relacionado eliminar de manera permanente un partido a la base de datos, dado el identificador
	 * 	del partido a eliminar.
	 * @param	id	Identificador del país.
	 */
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public void eliminarPartido(@PathVariable int id){
		servicio.eliminarPartido(id);
	}
}
