package com.aeditip.introspringbcd.wc2018.repositorios;

/**
 * Clase que define una estereotipo de repositorio estándar de Spring para la clase Jugador.
 * Historial de versiones
 *   1.0 (27/03/2021): Creación de la clase.
 *   1.1 (28/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.1
 */

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aeditip.introspringbcd.wc2018.entidades.Jugador;

// Anotación para anotar esta clase como un repositorio estereotipado de Spring para la entidad Jugador.
@Repository
public interface RepositorioJugador extends JpaRepository<Jugador, Integer> {
}
