package com.aeditip.introspringbcd.wc2018.dto;

/**
 * Clase que define un objeto enmascarado (mediante el patrón DTO) para la entidad Partido.
 * Historial de versiones
 *   1.0 (27/03/2021): Creación de la clase.
 *   1.1 (28/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.1
 */

import java.io.Serializable;

import com.aeditip.introspringbcd.wc2018.entidades.Partido;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// Anotaciones de la librería Lombok para la automatización de constructores y métodos iniciales.
@Data
@NoArgsConstructor
@AllArgsConstructor
// La siguiente línea permite suprimir la advertencia de declarar un número serial a una clase serializable.
@SuppressWarnings("serial")
public class DTOPartido implements Serializable {
	private int id;
	private int edicion;
	private int fase;
	private String codLocal;
	private String codVisitante;
	private DTOEstadisticasPartido estadisticas;
	
	/* Constructor de la clase, usando como parámetro un objeto de la entidad Partido.
	 * @param	partido		Objeto de entidad Partido para enmascarar.
	 */
	public DTOPartido(Partido partido) {
		this.id = partido.getId();
		this.edicion = partido.getOrganizacion().getEdicion().getAnho();
		this.fase = partido.getOrganizacion().getFase().getId();
		this.codLocal = partido.getLocal().getPais().getCodigo();
		this.codVisitante = partido.getVisitante().getPais().getCodigo();
		this.estadisticas = new DTOEstadisticasPartido(partido);
	}
}
