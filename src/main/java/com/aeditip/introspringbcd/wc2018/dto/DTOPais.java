package com.aeditip.introspringbcd.wc2018.dto;

/**
 * Clase que define un objeto enmascarado (mediante el patrón DTO) para la entidad País.
 * Historial de versiones
 *   1.0 (27/03/2021): Creación de la clase.
 *   1.1 (28/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.1
 */

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.aeditip.introspringbcd.wc2018.entidades.Edicion;
import com.aeditip.introspringbcd.wc2018.entidades.Pais;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//Anotaciones de la librería Lombok para la automatización de constructores y métodos iniciales.
@Data
@NoArgsConstructor
@AllArgsConstructor
// La siguiente línea permite suprimir la advertencia de declarar un número serial a una clase serializable.
@SuppressWarnings("serial")
public class DTOPais implements Serializable {
	private String codigo;
	private String denominacion;
	// Número de participaciones en las ediciones de la Copa Mundial.
	private int numParticipaciones = 0;
	// Nombre de la última participación en la que el país participó (la edición más reciente).
	private String ultimaParticipacion = "";
	
	/* Constructor de la clase, usando como parámetro un objeto de la entidad País.
	 * @param	pais	Objeto de entidad País para enmascarar.
	 */
	public DTOPais(Pais pais) {
		this.codigo = pais.getCodigo();
		this.denominacion = pais.getNombre();
		this.numParticipaciones = pais.getParticipaciones().size();
		// Ordenar por edición (más reciente a más antiguo)
		List<Edicion> participaciones = pais.getParticipaciones().stream()
				.map(x -> x.getEdicion()).collect(Collectors.toList());
		participaciones.sort(Comparator.comparingInt(Edicion::getAnho).reversed());
		this.ultimaParticipacion = participaciones.get(0).getNombre();
	}
}
