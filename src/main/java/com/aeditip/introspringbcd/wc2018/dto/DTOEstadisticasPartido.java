package com.aeditip.introspringbcd.wc2018.dto;

/**
 * Clase que define un objeto enmascarado (mediante el patrón DTO) para las estadísticas del partido, parte
 * 	de la entidad Partido.
 * Historial de versiones
 *   1.0 (27/03/2021): Creación de la clase.
 *   1.1 (28/03/2021): Agregación de comentarios para explicar los procedimientos utilizados.
 * @author Jorge Fatama Vera <jorge.fatama@pucp.edu.pe>
 * @version 1.1
 */

import java.io.Serializable;

import com.aeditip.introspringbcd.wc2018.entidades.Partido;
import com.aeditip.introspringbcd.wc2018.entidades.partido.ResultadoPais;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//Anotaciones de la librería Lombok para la automatización de constructores y métodos iniciales.
@Data
@NoArgsConstructor
@AllArgsConstructor
// La siguiente línea permite suprimir la advertencia de declarar un número serial a una clase serializable.
@SuppressWarnings("serial")
public class DTOEstadisticasPartido implements Serializable {
	private Estadistica local;
	private Estadistica visitante;
	
	// Clase interna para establecer las estadísticas de un país durante el partido.
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	private class Estadistica {
		private int goles;
		private int golesdePenal;
		private Tarjetas tarjetas;
		
		public Estadistica(ResultadoPais pais) {
			this.goles = pais.getGoles();
			this.golesdePenal = pais.getGolesPenal();
			this.tarjetas = new Tarjetas(pais);
		}
	}
	
	// Clase interna para establecer el número de tarjetas mostradas a jugadores de un país durante el partido.
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	private class Tarjetas {
		private int amarilla;
		private int segunda;
		private int roja;
		
		public Tarjetas(ResultadoPais pais) {
			this.amarilla = pais.getAmarillas();
			this.segunda = pais.getSegundas();
			this.roja = pais.getRojas();
		}
	}
	
	/* Constructor de la clase, usando como parámetro un objeto de la entidad Partido.
	 * @param	partido		Objeto de entidad Partido para enmascarar.
	 */
	public DTOEstadisticasPartido(Partido partido) {
		this.local = new Estadistica(partido.getLocal());
		this.visitante = new Estadistica(partido.getVisitante());
	}
}
